<?php


class calculadora  {

	public static function suma($a, $b)
	{
		return $a + $b; 
	}

	public static function resta($a, $b)
	{
		return $a - $b; 
	}

	public static function multiplicar($a, $b)
	{
		return $a * $b; 
	}

	public static function dividir($a, $b)
	{
		return $a / $b; 
	}

}


echo calculadora::suma(1,2);
echo calculadora::resta(1,2);
echo calculadora::multiplicar(1,2);
echo calculadora::dividir(1,2);